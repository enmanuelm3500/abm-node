var User = require('../models/usuario');

module.exports = {
    list: function(req, res, next) {
        User.find({}, (err, users) => {
            res.render('usuarios/index', { users: users });
        });
    },

    update_get: function(req, res, next) {
        User.findById(req.params.id, function(err, user) {
            res.render('usuarios/update', { errors: {}, user: user });
        });
    },

    update: function(req, res, next) {
        let update_values = { name: req.body.nombre };
        User.findByIdAndUpdate(req.params.id, update_values, function(err, user) {
            if (err) {
                console.log(err);
                res.render('usuarios/update', {
                    errors: err.errors,
                    user: new User({
                        name: req.body.name,
                        email: req.body.email,
                    }),
                });
            } else {
                res.redirect('/usuarios');
                return;
            }
        });
    },

    create_get: function(req, res, next) {
        res.render('usuarios/create', { errors: {}, user: new User() });
    },

    create: function(req, res, next) {
        if (req.body.password != req.body.confirm_password) {
            res.render('usuarios/create', {
                errors: {
                    confirm_password: { message: 'The passwords are incorret' },
                },
                user: new User({ 
                    name: req.body.name, 
                    email: req.body.email 
                }),
            });
            return;
        }

        User.create({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password,
            }, function(err, newUser) {
                if (err) {
                    console.log('el error', err);
                    res.render('usuarios/create', {
                        errors: err.errors,
                        user: new User({
                            name: req.body.name,
                            email: req.body.email,
                        })
                    });
                } else {
                    newUser.sendEmail();
                    res.redirect('/usuarios');
                }
            }
        );
    },
    
    delete: function(req, res, next) {
        User.findByIdAndDelete(req.body.id, function(err) {
            if (err) next(err);
            else res.redirect('/usuarios');
        });
    },
};