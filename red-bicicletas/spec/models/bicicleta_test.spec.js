var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

const MONGO_URI = "mongodb://localhost/testdb";

describe('TESTING BICICLETAS', () => {

    beforeAll((done) => {
        mongoose.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'falló mongoose'));
        db.once('open', function () {
            console.log('mongoose conectado');
            done();
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log('error al borrar todos');
            done();
        })
    });

    afterAll(() => {
        mongoose.connection.close();
    })

    it("Crea una instancia de bicicleta", () => {
        var bici = Bicicleta.createInstance(10, 'red', 'urban', [23, 23])
        expect(bici.code).toBe(10);
    })

    it('Agrega una bici a la BD', (done) => {
        let bici = Bicicleta.createInstance(11, 'green', 'urban', [1, 1]);
        bici.save(function (err, savedBici) {
            if (err) console.error(err);
            expect(savedBici.code).toEqual(bici.code)
            done()
        })
    })

    it('Encontrar una bici', async (done) => {
        let bici = Bicicleta.createInstance(11, 'green', 'urban', [1, 1]);
        let bici2 = Bicicleta.createInstance(12, 'red', 'urban', [2, 1]);
        await bici.save();
        await bici2.save();
        let biciEncontrada = await Bicicleta.findOne({code: 12});
        expect(biciEncontrada.code).toBe(12)
        done();
    })

    it('Eliminar una bici', async (done) => {
        let bici = Bicicleta.createInstance(11, 'green', 'urban', [1, 1]);
        await bici.save();
        await Bicicleta.deleteOne({code: 11})
        let biciEncontrada = await Bicicleta.findOne({code: 12});
        expect(biciEncontrada).toBeNull();
        done();
    })
})