var mongoose = require('mongoose');
var Reserva = require('../../models/reserva');
var Usuario = require('../../models/usuario');
var Bicicleta = require('../../models/bicicleta');

const MONGO_URI = "mongodb://localhost/testdb";

describe('TESTING RESERVA', () => {

    beforeAll((done) => {
        mongoose.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'falló conexion a la base de datos'));
        db.once('open', function () {
            console.log('mongoose conectado');
            done();
        });
    });

    afterEach((done) => {
        Reserva.deleteMany({}, (err, success) => {
            if (err) console.log('error al borrar todos');
            done();
        })
    });

    afterAll(() => {
        mongoose.connection.close();
    })

    it("Crea una instancia de reserva", async (done) => {
        // Crea y persiste el usuario y la bicicleta de la reserva
        var usuario = new Usuario({name: 'Jose'})
        let usuarioGuardado = await usuario.save();
        let bicicleta = new Bicicleta({code: 10, color: 'red', model: 'xp', ubicacion: [1,1]});
        let bicicletaGuardada = await bicicleta.save();
        // Crea y persiste la reserva
        var reserva = new Reserva({
            desde: new Date(2020, 1, 1),
            hasta: new Date(),
            usuario: usuarioGuardado._id,
            bicicleta: bicicletaGuardada._id
        });
        let reservaGuardada = await reserva.save();
        await reservaGuardada.populate('usuario').populate('bicicleta').execPopulate();
        expect(reservaGuardada.usuario.name).toEqual(usuario.name);
        expect(reservaGuardada.bicicleta.color).toEqual(bicicleta.color);
        done();
    });

    it("diasDeReserva()", async (done) => {
        // Crea el usuario y bici de la reserva
        var usuario = new Usuario({name: 'Pablo'})
        let bicicleta = new Bicicleta({code: 11, color: 'green', model: 'xp', ubicacion: [1,1]});
        await usuario.save();
        await bicicleta.save();
        // Crea  la reserva
        var reserva = new Reserva({
            desde: new Date(2020, 1, 1),
            hasta: new Date(2020, 1, 5),
            usuario: usuario._id,
            bicicleta: bicicleta._id
        });
        await reserva.save();
        expect(reserva.diasDeReserva()).toEqual(4);
        done();
    })
})