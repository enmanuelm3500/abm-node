var map = L.map('main_map').setView([-34.599844, -58.404518],13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

L.marker([-34.599844, -58.404518]).addTo(map);
L.marker([-34.589844, -58.404518]).addTo(map);
L.marker([-34.579844, -58.404518]).addTo(map);
