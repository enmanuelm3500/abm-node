var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

const passport = require('./config/passport');
const session = require('express-session');

const keys = require('./keys');

var bicicletaRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuarioAPIRouter = require('./routes/api/usuario');
var reservaAPIRouter = require('./routes/api/reserva');
var authAPIRouter = require('./routes/api/auth');


var usuarioRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');


const store = new session.MemoryStore;
var app = express();
app.set('secretKey', 'jwt_pwd_!!223344');

app.use(session({
  cookie: {maxAge: 240 * 60 *60* 2000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'juan'
}));



/* DB CONNECTION */
mongoose.connect(keys.MONGO_URI, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'falló mongoose'));
db.once('open', function() {
  console.log('App Mongoose conectada')
});

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(logger('dev'));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/bicicletas',loggedIn, bicicletaRouter);
app.use('/usuarios', usuarioRouter);
app.use('/token', tokenRouter);

app.use('/api/bicicletas',validarUsuario ,bicicletasAPIRouter);
app.use('/api/usuarios', usuarioAPIRouter);
app.use('/api/reservas', reservaAPIRouter);
app.use('/api/auth', authAPIRouter);


app.get('/login', function(req,res) {
  res.render('session/login');
});
app.post('/login', function(req,res,next){
  passport.authenticate('local', function(err, user, info){
    if(err) return next(err);
    if(!user) return res.render('session/login', {info});
    req.logIn(user, function (err){
      if (err) return next(err);
      return res.redirect('/');
    })
  })(req,res,next);
});
app.get('/logout', function(req,res) {
  req.logOut();
  res.redirect('/');
});


app.get('/forgotPassword', function(req,res,next){
  res.render('sessions/forgotPassword');
});
app.post('/forgotPassword', function(req,res,next){
  User.findOne({ email: req.body.email }, function(err, user) {
    if (!user) return res.render('sessions/forgotPassword', { info: { message: 'No existe el email para un usuario existente' } });

    user.resetPassword(function(err) {
      if (err) return next(err);
      console.log('sessions/forgotPasswordMessage');
    });

    res.render('sessions/forgotPasswordMessage');
  });
});

app.get('/resetPassword/:token', function(req, res, next) {
  console.log(req.params.token);
  token.findOne({ token: req.params.token }, function(err, token) {
    if(!token) return res.status(400).send({ msg: 'No existe un usuario asociado al token, verifique que su token no haya expirado' });
    User.findById(token._userId, function(err, user) {
      if(!user) return res.status(400).send({ msg: 'No existe un usuario asociado al token.' });
      res.render('sessions/resetPassword', {errors: { }, user: user});
    });
  });
});

app.post('/resetPassword', function(req, res) {
  if(req.body.password != req.body.confirm_password) {
    res.render('sessions/resetPassword', {errors: {confirm_password: {message: 'No coincide con el password ingresado'}}, user: new User({email: req.body.email})});
    return;
  }
  User.findOne({email: req.body.email}, function(err, user) {
    user.password = req.body.password;
    user.save(function(err) {
      if(err) {
        res.render('sessions/resetPassword', {errors: err.errors, user: new User({email: req.body.email})});
      } else {
        res.redirect('/login');
      }
    });
  });
});

function loggedIn(req, res, next) {
  if(req.user) {
    next();
  } else {
    console.log('Usuario sin loguearse');
    res.redirect('/login');
  }
};


// Index
app.get('/', (req, res) => {
  res.render('index')
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
    if (err) {
      console.log('Error de autenticacion');
      res.json({ status: "error", message: err.message, data: null });
    } else {
      console.log('usuario loggeado: ' + req.body.userId);
      req.body.userId = decoded.id;
      console.log('jwt verify: ' + decoded);
      next();
    }
  });
}

module.exports = app;
