var Bicicleta = require('../../models/bicicletas');

exports.bicicleta_list = async function (req, res) {
    Bicicleta.find({}, (err, docs) => {
        if (err) return res.status(400);
        return res.status(200).send(docs)
    });
}

exports.bicicleta_create = function (req, res) {
    let bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, req.body.ubicacion);
    bici.save((err, biciSaved) => {
        if (err) return res.status(400).send(err);
        return res.status(200).send(biciSaved);
    })
}

exports.bicicleta_delete = async function (req, res) {
    let res2 = await Bicicleta.deleteOne({ code: req.body.code });
    return (res2.deletedCount == 1) ? res.status(204).send() : res.status(404).send()
}

exports.bicicleta_update = async function (req, res) {
    let updatedDoc = await Bicicleta.findOneAndUpdate(
        { code: req.body.code },
        {
            color: req.body.color,
            modelo: req.body.modelo,
            ubicacion: req.body.ubicacion
        },
        {
            new: true
        }
    );
    res.status(200).send(updatedDoc);
}